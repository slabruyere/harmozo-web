(ns harmozo.note)

(def sharp-notes
  [:A :A# :B :C :C# :D :D# :E :F :F# :G :G#])

(def flat-notes
  [:A :Bb :B :C :Db :D :Eb :E :F :Gb :G :Ab])

(defn index
  "Returns the index of a note in a note list or nil"
  [notes note]
  (let [index (.indexOf notes note)]
    (when (> index -1)
      index)))

(defn find
  "Returns the index of the note along with the notes in the correct notation to go along with it."
  [note]
  (if-let [sharp-index (index sharp-notes note)]
    {:notes sharp-notes
     :index sharp-index}
    (when-let [flat-index (index flat-notes note)]
      {:notes flat-notes
       :index flat-index})))

(defn resolve
  "Resolves the name of a note as a function of its distance from the root and the note naming scheme."
  ([root-index note-index]
   (resolve sharp-notes root-index note-index))
  ([notes root-index note-index]
   (nth notes (mod (+ root-index note-index) 12))))

(defn sharp<->flat
  "Convert a note notation from sharp to flat.
  Throws an exception if note is in neither list."
  [note]
  (if-let [sharp-index (index sharp-notes note)]
    (nth flat-notes sharp-index)
    (let [flat-index (index flat-notes note)]
      (nth sharp-notes flat-index))))

(defn flatten
  [note]
  (let [{:keys [index notes]} (find note)]
    (nth notes (mod (- index 1) 12))))

(defn raise
  [note]
  (let [{:keys [index notes]} (find note)]
    (nth notes (mod (+ index 1) 12))))
