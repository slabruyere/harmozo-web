(ns harmozo.scale
  (:require [harmozo.note :as note]
            [harmozo.chord :as chord]))

(defn intervals->integers
  "Turn the well known cumulative intervals list into a list of integers from the root. Intervals are in half-steps to make things more simple, so a whole step is a 2, while a step is a 1. Hence 'whole whole half whole whole whole half' becomes [2 2 1 2 2 2 1]. But since the last intervals brings us back to the octave, we drop it to obtain the scale with no repetition of the root note."
  [intervals]
  (reduce
   (fn [acc val]
     (conj acc (+ (last acc) val)))
   [0]
   intervals))

(defn is-well-written?
  "A scale is well written if all the notes appear exactly once."
  [scale]
  (apply distinct?
         (map
          (comp first name)
          scale)))

(defn integers+root->notes
  "Resolve a full scale starting from a root note."
  [scale root]
  (let [{:keys [index notes]} (note/find root)
        result                (map
                               (partial note/resolve notes index)
                               scale)]
    (if (is-well-written? result)
      result
      (map note/sharp<->flat result))))

(defn notes+roman->chords
  [scale]
  (map chord/make (:notes scale) (:roman scale)))

(defn add-key-in-name
  [scale]
  (str (:name scale) " in the key of " (-> scale :root name)))

(defn make
  "Hydrate the scale objects with the correct integers, notes and chords"
  [scale root]
  (-> scale
      (assoc :root root)
      (#(assoc % :integers (intervals->integers (:intervals %))))
      (#(assoc % :notes (integers+root->notes (:integers %) root)))
      (#(assoc % :chords (notes+roman->chords %)))
      (#(assoc % :name (add-key-in-name %)))))
