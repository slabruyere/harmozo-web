(ns harmozo.select2
  (:require [cljsjs.select2]))

(defn on-change
  [progression]
  (fn [_]
    (this-as elem
      (let [new-val (js->clj
                     (.val
                      (js/$ elem)))]
        (when-not (= @progression new-val)
          (reset! progression new-val))))))

(defn prevent-tag-sorting
  "Inspired by an answer on StackOverflow
   https://stackoverflow.com/questions/31431197/select2-how-to-prevent-tags-sorting"
  [evt]
  (this-as elem
    (let [$option (-> evt
                      js->clj
                      (get-in ["params" "data" "element"])
                      clj->js
                      js/$)
          $elem   (js/$ elem)]
      (.detach $option)
      (.append $elem $option)
      (.trigger $elem "change"))))

(defn match-start
  "Inspired by the example in the documentation
   https://select2.org/searching"
  [params data]
  (if (empty? (.trim js/$ (aget params "term")))
    data
    (when (type (aget data "text"))
      (when (= (.indexOf (aget data "text")
                         (aget params "term"))
               0)
        (let [modified-data (.extend js/$ #js{} data true)]
          modified-data)))))

(defn init!
  [selector options value]
  (-> (.select2 (js/$ selector)
                (clj->js (assoc options :matcher match-start)))
      (.on "change" (on-change value))
      (.on "select2:select" prevent-tag-sorting)))
