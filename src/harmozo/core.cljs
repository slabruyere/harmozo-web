(ns harmozo.core
  (:require [reagent.core :as reagent :refer [atom]]
            [clojure.string :as s]
            [harmozo.util :as u]
            [harmozo.select2 :as select2]))

;; -------------------------
;; Home page

(def progression
  (atom []))

(defn display-scale [scale]
  [:div.scale
   {:key (str (:id scale) (:root scale))}
   [:<>
    [:div.name (:name scale)]
    [:div.notes (str "Notes: "  (s/join " " (map name (:notes scale))))]
    [:div.chords (str "Chords: " (s/join " " (map name (:chords scale))))]
    [:div.roman-progression (str "Roman Progression: " (s/join " " (map name (:roman-progression scale))))]
    [:div.functional-progression (str "Functional Progression: " (s/join " " (map name (:functional-progression scale))))]]])

(defn home-page []
  [:div#main
   [:div#content
    [:div#title
     [:h1 "Harmozō"]]
    [:div#select
     [:select#chords]]
    (when (> (count @progression) 1)      
      [:div#results
       (for [scale (u/find-scales @progression)]         
         (display-scale scale))])]])

;; -------------------------
;; Initialize app

(defn init! []
  (reagent/render [home-page]
                  (.getElementById js/document "app"))
  (select2/init! "select#chords"
                 {:placeholder            "What chords are you playing?"
                  :width                  "100%"
                  :multiple               true
                  :allowClear             true
                  :maximumSelectionLength 6
                  :data                   u/all-chords}
                 progression))

(init!)
