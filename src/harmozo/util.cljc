(ns harmozo.util
  "The goal is to have the tools in this namespace to be able to identify possible chord progressions from a list of chords / notes.
  The user enters a list of chords that we should match to a scale in order to determine the functions of each chord in that progression and suggest chords that would improve the chord progression."
  (:require [harmozo.scale :as scale]
            [harmozo.note :as note]
            [harmozo.chord :as chord]
            [harmozo.scales :as scales]))

(def all-notes
  (->> (concat note/sharp-notes note/flat-notes)
       distinct
       sort))

(def all-chords
  (mapcat
   (juxt chord/make-major chord/make-minor chord/make-diminished)
   all-notes))

(def all-scales
  (for [nt all-notes
        sc scales/raw-scales]
    (scale/make sc nt)))

(def resolutions
  '(:dominant->tonic))

(defn clean-progression
  [progression]
  (map keyword progression))

(defn match-scales
  [progression]
  (filter
   #(every? (into #{} (:chords %)) progression)
   all-scales))

(defn project-progression
  "When a progression matches a scale, we can check what functions of the scale are being used and find a good functional progression to suggest."
  [progression sc]
  (map #(.indexOf (:chords sc) %) progression))

(defn make-suggestions
  [functional-progression sc])

(defn make-report
  [progression sc]
  (let [indices                (project-progression progression sc)
        roman-progression      (map #(nth (:roman sc) %) indices)
        functional-progression (map #(nth (:functions sc) %) indices)
        suggestions            (make-suggestions functional-progression sc)]
    (merge (select-keys sc [:name :notes :chords])
           {:roman-progression      roman-progression
            :functional-progression functional-progression
            :suggestions            suggestions})))

(defn find-scales
  [progression]
  (let [cleaned-progression (clean-progression progression)
        matches             (match-scales cleaned-progression)]
    (map (partial make-report cleaned-progression) matches)))
