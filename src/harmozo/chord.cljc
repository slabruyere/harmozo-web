(ns harmozo.chord
  (:require [clojure.string :as s]
            [harmozo.note :as note]))

(defn append
  [suffix s]
  (str s suffix))

(defn make-major
  [note]
  note)

(defn make-minor
  [note]
  (-> note
      name
      ((partial append "m"))
      keyword))

(defn make-diminished
  [note]
  (-> note
      name
      ((partial append "dim"))
      keyword))

(defn is-diminished?
  [roman]
  (s/ends-with? (name roman) "°"))

(defn is-minor?
  [roman]
  (and (not (is-diminished? roman))
       (= roman
          (-> roman name s/lower-case keyword))))

(defn is-major?
  [roman]
  (and (not (is-diminished? roman))
       (= roman
          (-> roman name s/upper-case keyword))))

(defn is-flat?
  [roman]
  (s/starts-with? (name roman) "b"))

(defn is-raised?
  [roman]
  (s/starts-with? (name roman) "#"))

(defn get-type
  [chord]
  (cond
    (s/ends-with? (name chord) "dim") :diminished
    (s/ends-with? (name chord) "m")   :minor
    :else                             :major))

(defn get-note
  [chord]
  (-> chord
      name
      (s/replace #"(m|dim)$" "")
      keyword))

(defn flatten
  [chord]
  (let [flattened-note (note/flatten (get-note chord))]
    (case (get-type chord)
      :major      (keyword flattened-note)
      :minor      (make-minor flattened-note)
      :diminished (make-diminished flattened-note))))

(defn raise
  [chord]
  (let [raised-note (note/raise (get-note chord))]
    (case (get-type chord)
      :major      (keyword raised-note)
      :minor      (make-minor raised-note)
      :diminished (make-diminished raised-note))))

(defn make
  [note roman]
  (cond-> note 
    (is-diminished? roman) make-diminished
    (is-minor? roman)      make-minor
    (is-major? roman)      make-major
    (is-flat? roman)       flatten
    (is-raised? roman)     raise))

(defn read
  "Takes a raw input like Cminmaj7 and create an object like so
  {:note C :third :minor :fifth :major :seventh :major}
  allowing us to deduce the notes in that chord."
  [chord])

(defn write
  "Do the opposite of the read roman, i.e. take a chord object and write it into a human readable string."
  [chord])

#_(defn get-triad
    [chd]
    (let [note        (chord/get-note chd)
          major-scale (:notes (scale/make (first scales/raw-scales) note))
          triad       (list (first major-scale) (nth major-scale 2) (nth major-scale 4))]
      (case (chord/get-type chd)
        :major      triad
        :minor      (update triad 1 note/flatten)
        :diminished (-> triad
                        (update 1 note/flatten)
                        (update 2 note/flatten)))))
