(ns harmozo.scales)

;;   {:name      "major scale"
;;    :intervals [2 2 1 2 2 2]
;;    :integers  [0 2 4 5 7 9 11] => computed with 'intervals->integers'
;;    :notes     [:C :D :E :F :G :A :B] => computed with 'integers+root->notes'
;;    :roman [:I :ii :iii :IV :V :vi :vii°]
;;    :progressions []
;;    :chords    [:C :Dm :Em :F :G :Am :Bdim] => computed with 'notes+roman->chords'
;; })

(def raw-scales
  (list {:id        :major
         :name      "Major Scale"
         :quality   :major
         :intervals [2 2 1 2 2 2]
         :roman     [:I :ii :iii :IV :V :vi :vii°]
         :functions [:T :S :T :S :D :T :D]
         :examples  [[:I :IV :V :I]]}
        #_{:id        :dorian
           :name      "Dorian Mode"
           :quality   :major
           :intervals [2 1 2 2 2 1]
           :roman     [:i :ii :bIII :IV :v :vi° :bVII]}
        #_{:id        :phrygian
           :name      "Phrygian Mode"
           :quality   :major
           :intervals [1 2 2 2 1 2]
           :roman     [:i :bII :III :iv :v° :bVI :bvii]}
        #_{:id        :lydian
           :name      "Lydian Mode"
           :quality   :major
           :intervals [2 2 2 1 2 2]
           :roman     [:I :II :iii :#iv° :V :vi :vii]}
        #_{:id        :mixolydian
           :name      "Mixolydian Mode"
           :quality   :major
           :intervals [2 2 1 2 2 1]
           :roman     [:I :ii :iii° :IV :v :vi :bVII]}
        #_{:id        :aeolian
           :name      "Aeolian Mode (natural minor)"
           :quality   :major
           :intervals [2 1 2 2 1 2]
           :roman     [:i :ii° :bIII :iv :v :bVI :bVII]}
        #_{:id        :locrian
           :name      "Locrian Mode"
           :quality   :major
           :intervals [1 2 2 1 2 2]
           :roman     [:i° :bII :biii :iv :bV :bVI :bvii]}
        {:id        :minor
         :name      "Minor Scale"
         :quality   :minor
         :intervals [2 1 2 2 1 2]
         :roman     [:i :ii° :III :iv :v :VI :vii°]
         :functions [:T :S :T/S :S :D/S :T :D/S]}
        #_{:id        :harmonic-minor
           :name      "Harmonic Minor Scale"
           :quality   :minor
           :intervals [2 1 2 2 1 3]
           :roman     [:i :ii° :III :iv :v :VI :vii°]}
        #_{:id        :melodic-minor
           :name      "Melodic Minor Scale"
           :quality   :minor
           :intervals [2 1 2 2 2 2]
           :roman     [:i :ii° :III :iv :v :VI :vii°]}))
