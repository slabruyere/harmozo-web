# Harmozō

Harmozō helps you complete your chord progressions.

## Overview

> The term harmony derives from the Greek ἁρμονία harmonia, meaning "joint, agreement, concord" from the verb ἁρμόζω harmozō "fit together, join".
[Wikipedia - Harmony](https://en.wikipedia.org/wiki/Harmony#Etymology_and_definitions "Wikipedia - Harmony")

The aim of this app is to provide a very simple tool for people playing music in a band or alone, to create great progressions. Depending on the chord(s) you want to play, Harmozō can suggest additional chords depending on the emotion you are trying to convey.

## Development

To get an interactive development environment run:

    lein fig:build

This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

    (js/alert "Am I connected?")

and you should see an alert in the browser window.

To clean all compiled files:

	lein clean

To create a production build run:

	lein clean
	lein fig:min


## License

Copyright © 2019 Stéphane Labruyère

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
