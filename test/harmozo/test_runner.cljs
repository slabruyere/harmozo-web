(ns harmozo.test-runner
  (:require [figwheel.main.testing :refer [run-tests-async]]
            ;; require all the namespaces that we want to test
            [harmozo.chord-test]))

(defn -main [& args]
  (run-tests-async 5000))
