(ns harmozo.chord-test
  (:require  #?(:clj [clojure.test :refer [deftest testing is]]
                :cljs [cljs.test :refer-macros [deftest testing is]])
             [harmozo.chord :as chord]))

(deftest make-minor-test
  (testing "Should make chord minor"
    (is (= :C#m
           (chord/make-minor :C#)))))

(deftest make-diminished-test
  (testing "Should make chord diminished"
    (is (= :Bbdim
           (chord/make-diminished :Bb)))))

(deftest is-major?-test
  (testing "Should check that function chord is major"
    (is (chord/is-major? :I))
    (is (not (chord/is-major? :ii)))
    (is (not (chord/is-major? :vii°)))))

(deftest is-minor?-test
  (testing "Should check that function chord is minor"
    (is (not (chord/is-minor? :I)))
    (is (chord/is-minor? :ii))
    (is (not (chord/is-minor? :vii°)))))

(deftest is-diminished?-test
  (testing "Should check that function chord is diminished"
    (is (not (chord/is-diminished? :I)))
    (is (not (chord/is-diminished? :ii)))
    (is (chord/is-diminished? :vii°))))


#_(
   
   (deftest resolve-test
     (testing "Should resolve chord using note and function"
       (is (= :C (chord/resolve :C :I)))
       (is (= :Cm (chord/resolve :C :ii)))
       (is (= :Cdim (chord/resolve :C :vii°)))))

   (deftest get-type-test
     (testing "Should get chord type correctly"
       (is (= :major (chord/get-type :C)))
       (is (= :major (chord/get-type :C#)))
       (is (= :major (chord/get-type :Bb)))
       (is (= :minor (chord/get-type :Cm)))
       (is (= :minor (chord/get-type :C#m)))
       (is (= :minor (chord/get-type :Bbm)))
       (is (= :diminished (chord/get-type :Cdim)))
       (is (= :diminished (chord/get-type :C#dim)))
       (is (= :diminished (chord/get-type :Bbdim)))))

   (deftest get-note-test
     (testing "Should get note from chord"
       (is (= :C (chord/get-note :C)))
       (is (= :D (chord/get-note :Dm)))
       (is (= :Eb (chord/get-note :Ebdim)))))

   (deftest flatten-test
     (testing "Should flatten chord correctly"
       (is (= :A# (chord/flatten :B)))
       (is (= :A#m (chord/flatten :Bm)))
       (is (= :A#dim (chord/flatten :Bdim)))))

   (deftest raise-test
     (testing "Should raise chord correctly"
       (is (= :B (chord/raise :A#)))
       (is (= :Bm (chord/raise :A#m)))
       (is (= :Bdim (chord/raise :A#dim)))))

   (deftest get-triad-test
     (testing "Should get the right triad for chord"
       (is (= (list :C :E :G)
              (chord/get-triad :C)))))
   )
