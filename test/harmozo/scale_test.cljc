(ns harmozo.scale-test
  (:require #?(:clj [clojure.test :refer [deftest testing is]]
               :cljs [cljs.test :refer-macros [deftest testing is]])
            [harmozo.scale :as scale])  )
#_(
   (deftest resolve-test
     (testing "Should resolve major scale in C"
       (is (= (list :C :D :E :F :G :A :B)
              (scale/resolve scale/major :C)))))
   )
